/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <stdio.h>
#include <stdarg.h>

#ifndef E_PRINTF
#define E_PRINTF

int e_printf(char *format, ...);

int e_fprintf(FILE *stream, char *format, ...);

int e_sprintf(char *str, char *format, ...);

int e_vprintf( char *fomat, va_list ap);

int e_vfprintf(FILE *stream, char *format, va_list ap);

int e_vsprintf(char *str, char *format, va_list ap);

#endif /* E_PRINTF */
