/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <check.h>

#include "check_eprintf.h"

START_TEST(test_simple)
{
        char zero = 0, space = ' ', printable = 'a';

        check_printf("%c", zero);
        check_printf("%c", space);
        check_printf("%c", printable);
}
END_TEST

START_TEST(test_precision)
{
        char zero = 0, space = ' ', printable = 'a';

        check_printf("%.c", zero);
        check_printf("%.c", space);
        check_printf("%.c", printable);

        check_printf("%.0c", zero);
        check_printf("%.0c", space);
        check_printf("%.0c", printable);

        check_printf("%.1c", zero);
        check_printf("%.1c", space);
        check_printf("%.1c", printable);

        check_printf("%.10c", zero);
        check_printf("%.10c", space);
        check_printf("%.10c", printable);
}
END_TEST

START_TEST(test_field_width)
{
        char zero = 0, space = ' ', printable = 'a';

        check_printf("%1c", zero);
        check_printf("%1c", space);
        check_printf("%1c", printable);

        check_printf("%2c", zero);
        check_printf("%2c", space);
        check_printf("%2c", printable);
}
END_TEST

START_TEST(test_field_width_left_align)
{
        char zero = 0, space = ' ', printable = 'a';

        check_printf("%-c", zero);
        check_printf("%-c", space);
        check_printf("%-c", printable);

        check_printf("%-1c", zero);
        check_printf("%-1c", space);
        check_printf("%-1c", printable);

        check_printf("%-2c", zero);
        check_printf("%-2c", space);
        check_printf("%-2c", printable);
}
END_TEST

START_TEST(test_invalid_format)
{
        char zero = 0, space = ' ', printable = 'a';

        check_printf("%#c", zero);
        check_printf("%#c", space);
        check_printf("%#c", printable);

        check_printf("% c", zero);
        check_printf("% c", space);
        check_printf("% c", printable);

        check_printf("%0c", zero);
        check_printf("%0c", space);
        check_printf("%0c", printable);

        check_printf("%+c", zero);
        check_printf("%+c", space);
        check_printf("%+c", printable);
}
END_TEST

Suite *make_e_printf_char_suite(void)
{
        Suite *suite = suite_create("e_printf_char");
        TCase *test_core = tcase_create("e_printf_char");

        tcase_add_test(test_core, test_simple);
        tcase_add_test(test_core, test_precision);
        tcase_add_test(test_core, test_field_width);
        tcase_add_test(test_core, test_field_width_left_align);
        tcase_add_test(test_core, test_invalid_format);

        suite_add_tcase(suite, test_core);
        return suite;
}
