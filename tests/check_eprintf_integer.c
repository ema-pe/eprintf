/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <limits.h>

#include <check.h>

#include "check_eprintf.h"

START_TEST(test_simple_conversion)
{
        check_printf("%d", 159);
        check_printf("%d", INT_MAX);
        check_printf("%d", INT_MIN);
}
END_TEST

START_TEST(test_show_sign_flag)
{
        check_printf("%+d", 0);
        check_printf("%+d", 9);
        check_printf("%+d", 34);
}
END_TEST

START_TEST(test_initial_space_flag)
{
        check_printf("%d", 0);
        check_printf("% +d", 0);
}
END_TEST

START_TEST(test_short)
{
        const short zero = 0, pos = 165, neg = -43;

        check_printf("%hi", zero);
        check_printf("%hi", pos);
        check_printf("%hd", neg);
        check_printf("%hd", SHRT_MIN);
        check_printf("%hi", SHRT_MAX);
}
END_TEST

START_TEST(test_long)
{
        check_printf("%li", 0l);
        check_printf("%ld", 165l);
        check_printf("%li", -43l);
        check_printf("%ld", LONG_MAX);
        check_printf("%li", LONG_MIN);
}
END_TEST

START_TEST(test_alternate_output_flag)
{
        const long zero = 0l, pos = 174234l;

        check_printf("%#lu", zero);
        check_printf("%#lu", pos);
        check_printf("%#lo", zero);
        check_printf("%#lo", pos);
        check_printf("%#lx", zero);
        check_printf("%#lx", pos);
        check_printf("%#lX", zero);
        check_printf("%#lX", pos);
}
END_TEST

START_TEST(test_precision_long)
{
        const long zero = 0l, pos = 2576l, neg = -232l;

        check_printf("%.10d", zero);
        check_printf("%.10d", pos);
        check_printf("%.10d", neg);
        check_printf("%.3d", zero);
        check_printf("%.3d", pos);
        check_printf("%.3d", neg);
        check_printf("%.1d", zero);
        check_printf("%.1d", pos);
        check_printf("%.1d", neg);
        check_printf("%.0d", zero);
        check_printf("%.0d", pos);
        check_printf("%.0d", neg);
        check_printf("%.d", zero);
        check_printf("%.d", pos);
        check_printf("%.d", neg);
}
END_TEST

START_TEST(test_precision_unsigned)
{
        const unsigned long zero = 0ul, pos = 24364ul;

        check_printf("%.10lu", zero);
        check_printf("%.10lu", pos);
        check_printf("%.3lu", zero);
        check_printf("%.3lu", pos);
        check_printf("%.1lu", zero);
        check_printf("%.1lu", pos);
        check_printf("%.0lu", zero);
        check_printf("%.0lu", pos);
        check_printf("%.lu", zero);
        check_printf("%.lu", pos);

        check_printf("%.10lx", zero);
        check_printf("%.10lx", pos);
        check_printf("%.3lx", zero);
        check_printf("%.3lx", pos);
        check_printf("%.1lx", zero);
        check_printf("%.1lx", pos);
        check_printf("%.0lx", zero);
        check_printf("%.0lx", pos);
        check_printf("%.lx", zero);
        check_printf("%.lx", pos);

        check_printf("%.10lX", zero);
        check_printf("%.10lX", pos);
        check_printf("%.3lX", zero);
        check_printf("%.3lX", pos);
        check_printf("%.1lX", zero);
        check_printf("%.1lX", pos);
        check_printf("%.0lX", zero);
        check_printf("%.0lX", pos);
        check_printf("%.lX", zero);
        check_printf("%.lX", pos);

        check_printf("%.10lo", zero);
        check_printf("%.10lo", pos);
        check_printf("%.3lo", zero);
        check_printf("%.3lo", pos);
        check_printf("%.1lo", zero);
        check_printf("%.1lo", pos);
        check_printf("%.0lo", zero);
        check_printf("%.0lo", pos);
        check_printf("%.lo", zero);
        check_printf("%.lo", pos);
}
END_TEST

START_TEST(test_field_width_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%1ld", zero);
        check_printf("%1ld", pos);
        check_printf("%1ld", neg);

        check_printf("%2ld", zero);
        check_printf("%2ld", pos);
        check_printf("%2ld", neg);

        check_printf("%5ld", zero);
        check_printf("%5ld", pos);
        check_printf("%5ld", neg);

        check_printf("%7ld", zero);
        check_printf("%7ld", pos);
        check_printf("%7ld", neg);

        check_printf("%10ld", zero);
        check_printf("%10ld", pos);
        check_printf("%10ld", neg);
}
END_TEST

START_TEST(test_field_width_long_zero_pad)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%01ld", zero);
        check_printf("%01ld", pos);
        check_printf("%01ld", neg);

        check_printf("%02ld", zero);
        check_printf("%02ld", pos);
        check_printf("%02ld", neg);

        check_printf("%05ld", zero);
        check_printf("%05ld", pos);
        check_printf("%05ld", neg);

        check_printf("%07ld", zero);
        check_printf("%07ld", pos);
        check_printf("%07ld", neg);

        check_printf("%010ld", zero);
        check_printf("%010ld", pos);
        check_printf("%010ld", neg);
}
END_TEST

START_TEST(test_field_width_unsigned)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%1lu", zero);
        check_printf("%1lu", pos);
        check_printf("%2lu", zero);
        check_printf("%2lu", pos);
        check_printf("%5lu", zero);
        check_printf("%5lu", pos);
        check_printf("%7lu", zero);
        check_printf("%7lu", pos);
        check_printf("%10lu", zero);
        check_printf("%10lu", pos);

        check_printf("%1lo", zero);
        check_printf("%1lo", pos);
        check_printf("%2lo", zero);
        check_printf("%2lo", pos);
        check_printf("%5lo", zero);
        check_printf("%5lo", pos);
        check_printf("%7lo", zero);
        check_printf("%7lo", pos);
        check_printf("%10lo", zero);
        check_printf("%10lo", pos);

        check_printf("%1lx", zero);
        check_printf("%1lx", pos);
        check_printf("%2lx", zero);
        check_printf("%2lx", pos);
        check_printf("%5lx", zero);
        check_printf("%5lx", pos);
        check_printf("%7lx", zero);
        check_printf("%7lx", pos);
        check_printf("%10lx", zero);
        check_printf("%10lx", pos);

        check_printf("%1lX", zero);
        check_printf("%1lX", pos);
        check_printf("%2lX", zero);
        check_printf("%2lX", pos);
        check_printf("%5lX", zero);
        check_printf("%5lX", pos);
        check_printf("%7lX", zero);
        check_printf("%7lX", pos);
        check_printf("%10lX", zero);
        check_printf("%10lX", pos);
}
END_TEST

START_TEST(test_field_width_unsigned_zero_pad)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%01lu", zero);
        check_printf("%01lu", pos);
        check_printf("%02lu", zero);
        check_printf("%02lu", pos);
        check_printf("%05lu", zero);
        check_printf("%05lu", pos);
        check_printf("%07lu", zero);
        check_printf("%07lu", pos);
        check_printf("%010lu", zero);
        check_printf("%010lu", pos);

        check_printf("%01lo", zero);
        check_printf("%01lo", pos);
        check_printf("%02lo", zero);
        check_printf("%02lo", pos);
        check_printf("%05lo", zero);
        check_printf("%05lo", pos);
        check_printf("%07lo", zero);
        check_printf("%07lo", pos);
        check_printf("%010lo", zero);
        check_printf("%010lo", pos);

        check_printf("%01lx", zero);
        check_printf("%01lx", pos);
        check_printf("%02lx", zero);
        check_printf("%02lx", pos);
        check_printf("%05lx", zero);
        check_printf("%05lx", pos);
        check_printf("%07lx", zero);
        check_printf("%07lx", pos);
        check_printf("%010lx", zero);
        check_printf("%010lx", pos);

        check_printf("%01lX", zero);
        check_printf("%01lX", pos);
        check_printf("%02lX", zero);
        check_printf("%02lX", pos);
        check_printf("%05lX", zero);
        check_printf("%05lX", pos);
        check_printf("%07lX", zero);
        check_printf("%07lX", pos);
        check_printf("%010lX", zero);
        check_printf("%010lX", pos);
}
END_TEST

START_TEST(test_field_width_left_align_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%-1ld", zero);
        check_printf("%-1ld", pos);
        check_printf("%-1ld", neg);

        check_printf("%-2ld", zero);
        check_printf("%-2ld", pos);
        check_printf("%-2ld", neg);

        check_printf("%-5ld", zero);
        check_printf("%-5ld", pos);
        check_printf("%-5ld", neg);

        check_printf("%-7ld", zero);
        check_printf("%-7ld", pos);
        check_printf("%-7ld", neg);

        check_printf("%-10ld", zero);
        check_printf("%-10ld", pos);
        check_printf("%-10ld", neg);
}
END_TEST

START_TEST(test_field_width_left_align_long_zero_pad)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%-01ld", zero);
        check_printf("%-01ld", pos);
        check_printf("%-01ld", neg);

        check_printf("%-02ld", zero);
        check_printf("%-02ld", pos);
        check_printf("%-02ld", neg);

        check_printf("%-05ld", zero);
        check_printf("%-05ld", pos);
        check_printf("%-05ld", neg);

        check_printf("%-07ld", zero);
        check_printf("%-07ld", pos);
        check_printf("%-07ld", neg);

        check_printf("%-010ld", zero);
        check_printf("%-010ld", pos);
        check_printf("%-010ld", neg);
}
END_TEST

START_TEST(test_field_width_unsigned_left_align)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%-1lu", zero);
        check_printf("%-1lu", pos);
        check_printf("%-2lu", zero);
        check_printf("%-2lu", pos);
        check_printf("%-5lu", zero);
        check_printf("%-5lu", pos);
        check_printf("%-7lu", zero);
        check_printf("%-7lu", pos);
        check_printf("%-10lu", zero);
        check_printf("%-10lu", pos);

        check_printf("%-1lo", zero);
        check_printf("%-1lo", pos);
        check_printf("%-2lo", zero);
        check_printf("%-2lo", pos);
        check_printf("%-5lo", zero);
        check_printf("%-5lo", pos);
        check_printf("%-7lo", zero);
        check_printf("%-7lo", pos);
        check_printf("%-10lo", zero);
        check_printf("%-10lo", pos);

        check_printf("%-1lx", zero);
        check_printf("%-1lx", pos);
        check_printf("%-2lx", zero);
        check_printf("%-2lx", pos);
        check_printf("%-5lx", zero);
        check_printf("%-5lx", pos);
        check_printf("%-7lx", zero);
        check_printf("%-7lx", pos);
        check_printf("%-10lx", zero);
        check_printf("%-10lx", pos);

        check_printf("%-1lX", zero);
        check_printf("%-1lX", pos);
        check_printf("%-2lX", zero);
        check_printf("%-2lX", pos);
        check_printf("%-5lX", zero);
        check_printf("%-5lX", pos);
        check_printf("%-7lX", zero);
        check_printf("%-7lX", pos);
        check_printf("%-10lX", zero);
        check_printf("%-10lX", pos);
}
END_TEST

START_TEST(test_field_width_unsigned_zero_pad_left_align)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%-01lu", zero);
        check_printf("%-01lu", pos);
        check_printf("%-02lu", zero);
        check_printf("%-02lu", pos);
        check_printf("%-05lu", zero);
        check_printf("%-05lu", pos);
        check_printf("%-07lu", zero);
        check_printf("%-07lu", pos);
        check_printf("%-010lu", zero);
        check_printf("%-010lu", pos);

        check_printf("%-01lo", zero);
        check_printf("%-01lo", pos);
        check_printf("%-02lo", zero);
        check_printf("%-02lo", pos);
        check_printf("%-05lo", zero);
        check_printf("%-05lo", pos);
        check_printf("%-07lo", zero);
        check_printf("%-07lo", pos);
        check_printf("%-010lo", zero);
        check_printf("%-010lo", pos);

        check_printf("%-01lx", zero);
        check_printf("%-01lx", pos);
        check_printf("%-02lx", zero);
        check_printf("%-02lx", pos);
        check_printf("%-05lx", zero);
        check_printf("%-05lx", pos);
        check_printf("%-07lx", zero);
        check_printf("%-07lx", pos);
        check_printf("%-010lx", zero);
        check_printf("%-010lx", pos);

        check_printf("%-01lX", zero);
        check_printf("%-01lX", pos);
        check_printf("%-02lX", zero);
        check_printf("%-02lX", pos);
        check_printf("%-05lX", zero);
        check_printf("%-05lX", pos);
        check_printf("%-07lX", zero);
        check_printf("%-07lX", pos);
        check_printf("%-010lX", zero);
        check_printf("%-010lX", pos);
}
END_TEST

START_TEST(test_field_width_precision_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%10.ld", zero);
        check_printf("%10.ld", pos);
        check_printf("%10.ld", neg);
        check_printf("%5.ld", zero);
        check_printf("%5.ld", pos);
        check_printf("%5.ld", neg);
        check_printf("%2.ld", zero);
        check_printf("%2.ld", pos);
        check_printf("%2.ld", neg);
        check_printf("%1.ld", zero);
        check_printf("%1.ld", pos);
        check_printf("%1.ld", neg);

        check_printf("%10.1ld", zero);
        check_printf("%10.1ld", pos);
        check_printf("%10.1ld", neg);
        check_printf("%5.1ld", zero);
        check_printf("%5.1ld", pos);
        check_printf("%5.1ld", neg);
        check_printf("%2.1ld", zero);
        check_printf("%2.1ld", pos);
        check_printf("%2.1ld", neg);
        check_printf("%1.1ld", zero);
        check_printf("%1.1ld", pos);
        check_printf("%1.1ld", neg);

        check_printf("%10.5ld", zero);
        check_printf("%10.5ld", pos);
        check_printf("%10.5ld", neg);
        check_printf("%5.5ld", zero);
        check_printf("%5.5ld", pos);
        check_printf("%5.5ld", neg);
        check_printf("%2.5ld", zero);
        check_printf("%2.5ld", pos);
        check_printf("%2.5ld", neg);
        check_printf("%1.5ld", zero);
        check_printf("%1.5ld", pos);
        check_printf("%1.5ld", neg);

        check_printf("%10.10ld", zero);
        check_printf("%10.10ld", pos);
        check_printf("%10.10ld", neg);
        check_printf("%5.10ld", zero);
        check_printf("%5.10ld", pos);
        check_printf("%5.10ld", neg);
        check_printf("%2.10ld", zero);
        check_printf("%2.10ld", pos);
        check_printf("%2.10ld", neg);
        check_printf("%1.10ld", zero);
        check_printf("%1.10ld", pos);
        check_printf("%1.10ld", neg);
}
END_TEST

START_TEST(test_field_width_precision_left_align_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%-10.ld", zero);
        check_printf("%-10.ld", pos);
        check_printf("%-10.ld", neg);
        check_printf("%-5.ld", zero);
        check_printf("%-5.ld", pos);
        check_printf("%-5.ld", neg);
        check_printf("%-2.ld", zero);
        check_printf("%-2.ld", pos);
        check_printf("%-2.ld", neg);
        check_printf("%-1.ld", zero);
        check_printf("%-1.ld", pos);
        check_printf("%-1.ld", neg);

        check_printf("%-10.1ld", zero);
        check_printf("%-10.1ld", pos);
        check_printf("%-10.1ld", neg);
        check_printf("%-5.1ld", zero);
        check_printf("%-5.1ld", pos);
        check_printf("%-5.1ld", neg);
        check_printf("%-2.1ld", zero);
        check_printf("%-2.1ld", pos);
        check_printf("%-2.1ld", neg);
        check_printf("%-1.1ld", zero);
        check_printf("%-1.1ld", pos);
        check_printf("%-1.1ld", neg);

        check_printf("%-10.5ld", zero);
        check_printf("%-10.5ld", pos);
        check_printf("%-10.5ld", neg);
        check_printf("%-5.5ld", zero);
        check_printf("%-5.5ld", pos);
        check_printf("%-5.5ld", neg);
        check_printf("%-2.5ld", zero);
        check_printf("%-2.5ld", pos);
        check_printf("%-2.5ld", neg);
        check_printf("%-1.5ld", zero);
        check_printf("%-1.5ld", pos);
        check_printf("%-1.5ld", neg);

        check_printf("%-10.10ld", zero);
        check_printf("%-10.10ld", pos);
        check_printf("%-10.10ld", neg);
        check_printf("%-5.10ld", zero);
        check_printf("%-5.10ld", pos);
        check_printf("%-5.10ld", neg);
        check_printf("%-2.10ld", zero);
        check_printf("%-2.10ld", pos);
        check_printf("%-2.10ld", neg);
        check_printf("%-1.10ld", zero);
        check_printf("%-1.10ld", pos);
        check_printf("%-1.10ld", neg);
}
END_TEST

START_TEST(test_field_width_precision_zero_pad_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("%010.ld", zero);
        check_printf("%010.ld", pos);
        check_printf("%010.ld", neg);
        check_printf("%05.ld", zero);
        check_printf("%05.ld", pos);
        check_printf("%05.ld", neg);
        check_printf("%02.ld", zero);
        check_printf("%02.ld", pos);
        check_printf("%02.ld", neg);
        check_printf("%01.ld", zero);
        check_printf("%01.ld", pos);
        check_printf("%01.ld", neg);

        check_printf("%010.1ld", zero);
        check_printf("%010.1ld", pos);
        check_printf("%010.1ld", neg);
        check_printf("%05.1ld", zero);
        check_printf("%05.1ld", pos);
        check_printf("%05.1ld", neg);
        check_printf("%02.1ld", zero);
        check_printf("%02.1ld", pos);
        check_printf("%02.1ld", neg);
        check_printf("%01.1ld", zero);
        check_printf("%01.1ld", pos);
        check_printf("%01.1ld", neg);

        check_printf("%010.5ld", zero);
        check_printf("%010.5ld", pos);
        check_printf("%010.5ld", neg);
        check_printf("%05.5ld", zero);
        check_printf("%05.5ld", pos);
        check_printf("%05.5ld", neg);
        check_printf("%02.5ld", zero);
        check_printf("%02.5ld", pos);
        check_printf("%02.5ld", neg);
        check_printf("%01.5ld", zero);
        check_printf("%01.5ld", pos);
        check_printf("%01.5ld", neg);

        check_printf("%010.10ld", zero);
        check_printf("%010.10ld", pos);
        check_printf("%010.10ld", neg);
        check_printf("%05.10ld", zero);
        check_printf("%05.10ld", pos);
        check_printf("%05.10ld", neg);
        check_printf("%02.10ld", zero);
        check_printf("%02.10ld", pos);
        check_printf("%02.10ld", neg);
        check_printf("%01.10ld", zero);
        check_printf("%01.10ld", pos);
        check_printf("%01.10ld", neg);
}
END_TEST


START_TEST(test_field_width_precision_unsigned)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%1.lu", zero);
        check_printf("%1.lu", pos);
        check_printf("%1.lo", zero);
        check_printf("%1.lo", pos);
        check_printf("%1.lx", zero);
        check_printf("%1.lx", pos);
        check_printf("%1.lX", zero);
        check_printf("%1.lX", pos);

        check_printf("%1.1lu", zero);
        check_printf("%1.1lu", pos);
        check_printf("%1.1lo", zero);
        check_printf("%1.1lo", pos);
        check_printf("%1.1lx", zero);
        check_printf("%1.1lx", pos);
        check_printf("%1.1lX", zero);
        check_printf("%1.1lX", pos);

        check_printf("%5.2lu", zero);
        check_printf("%5.2lu", pos);
        check_printf("%5.2lo", zero);
        check_printf("%5.2lo", pos);
        check_printf("%5.2lx", zero);
        check_printf("%5.2lx", pos);
        check_printf("%5.2lX", zero);
        check_printf("%5.2lX", pos);

        check_printf("%10.lu", zero);
        check_printf("%10.lu", pos);
        check_printf("%10.lo", zero);
        check_printf("%10.lo", pos);
        check_printf("%10.lx", zero);
        check_printf("%10.lx", pos);
        check_printf("%10.lX", zero);
        check_printf("%10.lX", pos);

        check_printf("%10.10lu", zero);
        check_printf("%10.10lu", pos);
        check_printf("%10.10lo", zero);
        check_printf("%10.10lo", pos);
        check_printf("%10.10lx", zero);
        check_printf("%10.10lx", pos);
        check_printf("%10.10lX", zero);
        check_printf("%10.10lX", pos);
}
END_TEST

START_TEST(test_field_width_precision_left_align_unsigned)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%-1.lu", zero);
        check_printf("%-1.lu", pos);
        check_printf("%-1.lo", zero);
        check_printf("%-1.lo", pos);
        check_printf("%-1.lx", zero);
        check_printf("%-1.lx", pos);
        check_printf("%-1.lX", zero);
        check_printf("%-1.lX", pos);

        check_printf("%-1.1lu", zero);
        check_printf("%-1.1lu", pos);
        check_printf("%-1.1lo", zero);
        check_printf("%-1.1lo", pos);
        check_printf("%-1.1lx", zero);
        check_printf("%-1.1lx", pos);
        check_printf("%-1.1lX", zero);
        check_printf("%-1.1lX", pos);

        check_printf("%-5.2lu", zero);
        check_printf("%-5.2lu", pos);
        check_printf("%-5.2lo", zero);
        check_printf("%-5.2lo", pos);
        check_printf("%-5.2lx", zero);
        check_printf("%-5.2lx", pos);
        check_printf("%-5.2lX", zero);
        check_printf("%-5.2lX", pos);

        check_printf("%-10.lu", zero);
        check_printf("%-10.lu", pos);
        check_printf("%-10.lo", zero);
        check_printf("%-10.lo", pos);
        check_printf("%-10.lx", zero);
        check_printf("%-10.lx", pos);
        check_printf("%-10.lX", zero);
        check_printf("%-10.lX", pos);

        check_printf("%-10.10lu", zero);
        check_printf("%-10.10lu", pos);
        check_printf("%-10.10lo", zero);
        check_printf("%-10.10lo", pos);
        check_printf("%-10.10lx", zero);
        check_printf("%-10.10lx", pos);
        check_printf("%-10.10lX", zero);
        check_printf("%-10.10lX", pos);
}
END_TEST

START_TEST(test_field_width_precision_zero_pad_unsigned)
{
        const unsigned long zero = 0ul, pos = 25476ul;

        check_printf("%01.lu", zero);
        check_printf("%01.lu", pos);
        check_printf("%01.lo", zero);
        check_printf("%01.lo", pos);
        check_printf("%01.lx", zero);
        check_printf("%01.lx", pos);
        check_printf("%01.lX", zero);
        check_printf("%01.lX", pos);

        check_printf("%01.1lu", zero);
        check_printf("%01.1lu", pos);
        check_printf("%01.1lo", zero);
        check_printf("%01.1lo", pos);
        check_printf("%01.1lx", zero);
        check_printf("%01.1lx", pos);
        check_printf("%01.1lX", zero);
        check_printf("%01.1lX", pos);

        check_printf("%05.2lu", zero);
        check_printf("%05.2lu", pos);
        check_printf("%05.2lo", zero);
        check_printf("%05.2lo", pos);
        check_printf("%05.2lx", zero);
        check_printf("%05.2lx", pos);
        check_printf("%05.2lX", zero);
        check_printf("%05.2lX", pos);

        check_printf("%010.lu", zero);
        check_printf("%010.lu", pos);
        check_printf("%010.lo", zero);
        check_printf("%010.lo", pos);
        check_printf("%010.lx", zero);
        check_printf("%010.lx", pos);
        check_printf("%010.lX", zero);
        check_printf("%010.lX", pos);

        check_printf("%010.10lu", zero);
        check_printf("%010.10lu", pos);
        check_printf("%010.10lo", zero);
        check_printf("%010.10lo", pos);
        check_printf("%010.10lx", zero);
        check_printf("%010.10lx", pos);
        check_printf("%010.10lX", zero);
        check_printf("%010.10lX", pos);
}
END_TEST

START_TEST(test_field_width_initial_space_long)
{
        const long zero = 0l, pos = 25476l, neg = -99213l;

        check_printf("% 1ld", zero);
        check_printf("% 1ld", pos);
        check_printf("% 1ld", neg);

        check_printf("% 2ld", zero);
        check_printf("% 2ld", pos);
        check_printf("% 2ld", neg);

        check_printf("% 5ld", zero);
        check_printf("% 5ld", pos);
        check_printf("% 5ld", neg);

        check_printf("% 7ld", zero);
        check_printf("% 7ld", pos);
        check_printf("% 7ld", neg);

        check_printf("% 10ld", zero);
        check_printf("% 10ld", pos);
        check_printf("% 10ld", neg);

        /* '+' overrides ' '. */
        check_printf("%+ 1ld", zero);
        check_printf("%+ 1ld", pos);
        check_printf("%+ 1ld", neg);

        check_printf("%+ 2ld", zero);
        check_printf("%+ 2ld", pos);
        check_printf("%+ 2ld", neg);

        check_printf("%+ 5ld", zero);
        check_printf("%+ 5ld", pos);
        check_printf("%+ 5ld", neg);

        check_printf("%+ 7ld", zero);
        check_printf("%+ 7ld", pos);
        check_printf("%+ 7ld", neg);

        check_printf("%+ 10ld", zero);
        check_printf("%+ 10ld", pos);
        check_printf("%+ 10ld", neg);

        /* Left align should not change the behaviour. */
        check_printf("%- 1ld", zero);
        check_printf("%- 1ld", pos);
        check_printf("%- 1ld", neg);

        check_printf("%- 2ld", zero);
        check_printf("%- 2ld", pos);
        check_printf("%- 2ld", neg);

        check_printf("%- 5ld", zero);
        check_printf("%- 5ld", pos);
        check_printf("%- 5ld", neg);

        check_printf("%- 7ld", zero);
        check_printf("%- 7ld", pos);
        check_printf("%- 7ld", neg);

        check_printf("%- 10ld", zero);
        check_printf("%- 10ld", pos);
        check_printf("%- 10ld", neg);
}
END_TEST

Suite *make_e_printf_integer_suite(void)
{
        Suite *suite = suite_create("e_printf_integer");
        TCase *test_core = tcase_create("e_printf_integer");

        tcase_add_test(test_core, test_simple_conversion);
        tcase_add_test(test_core, test_show_sign_flag);
        tcase_add_test(test_core, test_initial_space_flag);
        tcase_add_test(test_core, test_short);
        tcase_add_test(test_core, test_long);
        tcase_add_test(test_core, test_alternate_output_flag);
        tcase_add_test(test_core, test_precision_long);
        tcase_add_test(test_core, test_precision_unsigned);
        tcase_add_test(test_core, test_field_width_long);
        tcase_add_test(test_core, test_field_width_long_zero_pad);
        tcase_add_test(test_core, test_field_width_unsigned);
        tcase_add_test(test_core, test_field_width_unsigned_zero_pad);
        tcase_add_test(test_core, test_field_width_unsigned_zero_pad_left_align);
        tcase_add_test(test_core, test_field_width_unsigned_left_align);
        tcase_add_test(test_core, test_field_width_left_align_long_zero_pad);
        tcase_add_test(test_core, test_field_width_left_align_long);
        tcase_add_test(test_core, test_field_width_precision_long);
        tcase_add_test(test_core, test_field_width_precision_left_align_long);
        tcase_add_test(test_core, test_field_width_precision_zero_pad_long);
        tcase_add_test(test_core, test_field_width_precision_unsigned);
        tcase_add_test(test_core, test_field_width_precision_left_align_unsigned);
        tcase_add_test(test_core, test_field_width_precision_zero_pad_unsigned);
        tcase_add_test(test_core, test_field_width_initial_space_long);

        suite_add_tcase(suite, test_core);

        return suite;
}
