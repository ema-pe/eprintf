/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <check.h>

#include "check_eprintf.h"

START_TEST(test_simple)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%s", str);
        check_printf("%s", empty);
        check_printf("%s", single);
}
END_TEST

START_TEST(test_precision)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%.s", str);
        check_printf("%.s", empty);
        check_printf("%.s", single);

        check_printf("%.0s", str);
        check_printf("%.0s", empty);
        check_printf("%.0s", single);

        check_printf("%.1s", str);
        check_printf("%.1s", empty);
        check_printf("%.1s", single);

        check_printf("%.9s", str);
        check_printf("%.9s", empty);
        check_printf("%.9s", single);

        check_printf("%.20s", str);
        check_printf("%.20s", empty);
        check_printf("%.20s", single);
}
END_TEST

START_TEST(test_precision_left_align)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%-.s", str);
        check_printf("%-.s", empty);
        check_printf("%-.s", single);

        check_printf("%-.0s", str);
        check_printf("%-.0s", empty);
        check_printf("%-.0s", single);

        check_printf("%-.1s", str);
        check_printf("%-.1s", empty);
        check_printf("%-.1s", single);

        check_printf("%-.9s", str);
        check_printf("%-.9s", empty);
        check_printf("%-.9s", single);

        check_printf("%-.20s", str);
        check_printf("%-.20s", empty);
        check_printf("%-.20s", single);
}
END_TEST

START_TEST(test_field_width)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%1s", str);
        check_printf("%1s", empty);
        check_printf("%1s", single);

        check_printf("%2s", str);
        check_printf("%2s", empty);
        check_printf("%2s", single);

        check_printf("%3s", str);
        check_printf("%3s", empty);
        check_printf("%3s", single);

        check_printf("%10s", str);
        check_printf("%10s", empty);
        check_printf("%10s", single);

        check_printf("%15s", str);
        check_printf("%15s", empty);
        check_printf("%15s", single);
}
END_TEST

START_TEST(test_field_width_left_align)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%-1s", str);
        check_printf("%-1s", empty);
        check_printf("%-1s", single);

        check_printf("%-2s", str);
        check_printf("%-2s", empty);
        check_printf("%-2s", single);

        check_printf("%-3s", str);
        check_printf("%-3s", empty);
        check_printf("%-3s", single);

        check_printf("%-10s", str);
        check_printf("%-10s", empty);
        check_printf("%-10s", single);

        check_printf("%-15s", str);
        check_printf("%-15s", empty);
        check_printf("%-15s", single);
}
END_TEST

START_TEST(test_field_width_precision)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%1.s", str);
        check_printf("%1.s", empty);
        check_printf("%1.s", single);
        check_printf("%1.0s", str);
        check_printf("%1.0s", empty);
        check_printf("%1.0s", single);
        check_printf("%1.1s", str);
        check_printf("%1.1s", empty);
        check_printf("%1.1s", single);
        check_printf("%1.9s", str);
        check_printf("%1.9s", empty);
        check_printf("%1.9s", single);
        check_printf("%1.20s", str);
        check_printf("%1.20s", empty);
        check_printf("%1.20s", single);

        check_printf("%2.s", str);
        check_printf("%2.s", empty);
        check_printf("%2.s", single);
        check_printf("%2.0s", str);
        check_printf("%2.0s", empty);
        check_printf("%2.0s", single);
        check_printf("%2.1s", str);
        check_printf("%2.1s", empty);
        check_printf("%2.1s", single);
        check_printf("%2.9s", str);
        check_printf("%2.9s", empty);
        check_printf("%2.9s", single);
        check_printf("%2.20s", str);
        check_printf("%2.20s", empty);
        check_printf("%2.20s", single);

        check_printf("%3.s", str);
        check_printf("%3.s", empty);
        check_printf("%3.s", single);
        check_printf("%3.0s", str);
        check_printf("%3.0s", empty);
        check_printf("%3.0s", single);
        check_printf("%3.1s", str);
        check_printf("%3.1s", empty);
        check_printf("%3.1s", single);
        check_printf("%3.9s", str);
        check_printf("%3.9s", empty);
        check_printf("%3.9s", single);
        check_printf("%3.20s", str);
        check_printf("%3.20s", empty);
        check_printf("%3.20s", single);

        check_printf("%10.s", str);
        check_printf("%10.s", empty);
        check_printf("%10.s", single);
        check_printf("%10.0s", str);
        check_printf("%10.0s", empty);
        check_printf("%10.0s", single);
        check_printf("%10.1s", str);
        check_printf("%10.1s", empty);
        check_printf("%10.1s", single);
        check_printf("%10.9s", str);
        check_printf("%10.9s", empty);
        check_printf("%10.9s", single);
        check_printf("%10.20s", str);
        check_printf("%10.20s", empty);
        check_printf("%10.20s", single);

        check_printf("%15.s", str);
        check_printf("%15.s", empty);
        check_printf("%15.s", single);
        check_printf("%15.0s", str);
        check_printf("%15.0s", empty);
        check_printf("%15.0s", single);
        check_printf("%15.1s", str);
        check_printf("%15.1s", empty);
        check_printf("%15.1s", single);
        check_printf("%15.9s", str);
        check_printf("%15.9s", empty);
        check_printf("%15.9s", single);
        check_printf("%15.20s", str);
        check_printf("%15.20s", empty);
        check_printf("%15.20s", single);
}
END_TEST

START_TEST(test_field_width_precision_left_align)
{
        char str[] = "Hello World!", empty[] = "", single[] = "a";

        check_printf("%-1.s", str);
        check_printf("%-1.s", empty);
        check_printf("%-1.s", single);
        check_printf("%-1.0s", str);
        check_printf("%-1.0s", empty);
        check_printf("%-1.0s", single);
        check_printf("%-1.1s", str);
        check_printf("%-1.1s", empty);
        check_printf("%-1.1s", single);
        check_printf("%-1.9s", str);
        check_printf("%-1.9s", empty);
        check_printf("%-1.9s", single);
        check_printf("%-1.20s", str);
        check_printf("%-1.20s", empty);
        check_printf("%-1.20s", single);

        check_printf("%-2.s", str);
        check_printf("%-2.s", empty);
        check_printf("%-2.s", single);
        check_printf("%-2.0s", str);
        check_printf("%-2.0s", empty);
        check_printf("%-2.0s", single);
        check_printf("%-2.1s", str);
        check_printf("%-2.1s", empty);
        check_printf("%-2.1s", single);
        check_printf("%-2.9s", str);
        check_printf("%-2.9s", empty);
        check_printf("%-2.9s", single);
        check_printf("%-2.20s", str);
        check_printf("%-2.20s", empty);
        check_printf("%-2.20s", single);

        check_printf("%-3.s", str);
        check_printf("%-3.s", empty);
        check_printf("%-3.s", single);
        check_printf("%-3.0s", str);
        check_printf("%-3.0s", empty);
        check_printf("%-3.0s", single);
        check_printf("%-3.1s", str);
        check_printf("%-3.1s", empty);
        check_printf("%-3.1s", single);
        check_printf("%-3.9s", str);
        check_printf("%-3.9s", empty);
        check_printf("%-3.9s", single);
        check_printf("%-3.20s", str);
        check_printf("%-3.20s", empty);
        check_printf("%-3.20s", single);

        check_printf("%-10.s", str);
        check_printf("%-10.s", empty);
        check_printf("%-10.s", single);
        check_printf("%-10.0s", str);
        check_printf("%-10.0s", empty);
        check_printf("%-10.0s", single);
        check_printf("%-10.1s", str);
        check_printf("%-10.1s", empty);
        check_printf("%-10.1s", single);
        check_printf("%-10.9s", str);
        check_printf("%-10.9s", empty);
        check_printf("%-10.9s", single);
        check_printf("%-10.20s", str);
        check_printf("%-10.20s", empty);
        check_printf("%-10.20s", single);

        check_printf("%-15.s", str);
        check_printf("%-15.s", empty);
        check_printf("%-15.s", single);
        check_printf("%-15.0s", str);
        check_printf("%-15.0s", empty);
        check_printf("%-15.0s", single);
        check_printf("%-15.1s", str);
        check_printf("%-15.1s", empty);
        check_printf("%-15.1s", single);
        check_printf("%-15.9s", str);
        check_printf("%-15.9s", empty);
        check_printf("%-15.9s", single);
        check_printf("%-15.20s", str);
        check_printf("%-15.20s", empty);
        check_printf("%-15.20s", single);
}
END_TEST

Suite *make_e_printf_str_suite(void)
{
        Suite *suite = suite_create("e_printf_str");
        TCase *test_core = tcase_create("e_printf_str");

        tcase_add_test(test_core, test_simple);
        tcase_add_test(test_core, test_precision);
        tcase_add_test(test_core, test_precision_left_align);
        tcase_add_test(test_core, test_field_width);
        tcase_add_test(test_core, test_field_width_left_align);
        tcase_add_test(test_core, test_field_width_precision);
        tcase_add_test(test_core, test_field_width_precision_left_align);

        suite_add_tcase(suite, test_core);
        return suite;
}
