/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#ifndef CHECK_E_PRINTF
#define CHECK_E_PRINTF

#define CHECK_BUF_SIZE 1024

Suite *make_e_printf_integer_suite(void);

Suite *make_e_printf_str_suite(void);

Suite *make_e_printf_char_suite(void);

Suite *make_e_printf_float_suite(void);

Suite *make_e_printf_special_suite(void);

/*
 * check_printf: checks a call of my printf implementation with format "format"
 * against local printf implementation.
 */
void check_printf(char *format, ...);

/*
 * check_printf_manual: checks a call of my printf implementation with format
 * "format" against a custom expected result.
 */
void check_printf_manual(char *expected, char *format, ...);

#endif /* CHECK_E_PRINTF */
