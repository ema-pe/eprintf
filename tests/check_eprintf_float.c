/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <math.h>

#include <check.h>

#include "check_eprintf.h"

/*
 * Big warning: I didn't implement rounding. So when I check my version with
 * standard implementation it will not work, so I need to check against a custom
 * string.
 */

/*
 * On platforms with infinity "HUGE_VAL" and "-HUGE_VAL" are "-inf" and "+inf".
 */
const double pos = 24.16, pos_int = 25., neg_int = -25., neg = -24.16,
             pos_zero = +0., neg_zero = -0., pos_inf = HUGE_VAL,
             neg_inf = -HUGE_VAL;

START_TEST(test_normal_conversion)
{
        check_printf("%f", pos);
        check_printf("%f", pos_int);
        check_printf("%f", neg_int);
        check_printf("%f", neg);

        check_printf("%f", 0.3);

        check_printf("%f", pos_zero);
        check_printf("%f", neg_zero);
}
END_TEST

START_TEST(test_normal_show_sign)
{
        check_printf("%+f", pos);
        check_printf("%+f", pos_int);
        check_printf("%+f", neg_int);
        check_printf("%+f", neg);
        check_printf("%+f", pos_zero);
        check_printf("%+f", neg_zero);
}
END_TEST

START_TEST(test_normal_initial_space)
{
        check_printf("% f", pos);
        check_printf("% f", pos_int);
        check_printf("% f", neg_int);
        check_printf("% f", neg);
        check_printf("% f", pos_zero);
        check_printf("% f", neg_zero);

        /* Test that '+' overrides ' '. */
        check_printf("% +f", pos);
        check_printf("% +f", pos_int);
        check_printf("% +f", neg_int);
        check_printf("% +f", neg);
        check_printf("% +f", pos_zero);
        check_printf("% +f", neg_zero);
}
END_TEST

START_TEST(test_normal_alternative_output)
{
        check_printf("%#f", pos);
        check_printf("%#f", pos_int);
        check_printf("%#f", neg_int);
        check_printf("%#f", neg);
        check_printf("%#f", pos_zero);
        check_printf("%#f", neg_zero);

        check_printf("%#.f", pos);
        check_printf("%#.f", pos_int);
        check_printf("%#.f", neg_int);
        check_printf("%#.f", neg);
        check_printf("%#.f", pos_zero);
        check_printf("%#.f", neg_zero);
}
END_TEST

START_TEST(test_normal_precision)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999;

        /*
         * I can't compare to local "printf" implementation because my
         * implementation doesn't do rounding.
         */
        check_printf("%f", pos);
        check_printf_manual("-23432.234209", "%f", neg);
        check_printf_manual("0.334322", "%f", fp_frac);
        check_printf("%f", fp_int);
        check_printf("%f", big);
        check_printf("%f", neg_big);
        check_printf_manual("23.239999", "%f", nine);

        check_printf("%.f", pos);
        check_printf("%.f", neg);
        check_printf("%.f", fp_frac);
        check_printf("%.f", fp_int);
        check_printf("%.f", big);
        check_printf("%.f", neg_big);
        check_printf("%.f", nine);

        check_printf("%.1f", pos);
        check_printf("%.1f", neg);
        check_printf("%.1f", fp_frac);
        check_printf("%.1f", fp_int);
        check_printf("%.1f", big);
        check_printf("%.1f", neg_big);
        check_printf("%.1f", nine);

        check_printf("%.5f", pos);
        check_printf_manual("-23432.23420", "%.5f", neg);
        check_printf_manual("0.33432", "%.5f", fp_frac);
        check_printf("%.5f", fp_int);
        check_printf("%.5f", big);
        check_printf("%.5f", neg_big);
        check_printf_manual("23.23999", "%.5f", nine);

        check_printf("%.10f", pos);
        check_printf_manual("-23432.2342099999", "%.10f", neg);
        check_printf("%.10f", fp_frac);
        check_printf("%.10f", fp_int);
        check_printf("%.10f", big);
        check_printf("%.10f", neg_big);
        check_printf_manual("23.2399999999", "%.10f", nine);

        check_printf("%.56f", pos);
        check_printf("%.56f", neg);
        check_printf_manual("0.33432253200000001669422999839298427104949951171875000000", "%.56f", fp_frac);
        check_printf("%.56f", fp_int);
        check_printf("%.56f", big);
        check_printf("%.56f", neg_big);
        check_printf("%.56f", nine);
}
END_TEST

START_TEST(test_normal_field_width)
{
        check_printf("%1f", pos);
        check_printf("%1f", pos_int);
        check_printf("%1f", neg_int);
        check_printf("%1f", neg);
        check_printf("%1f", pos_zero);
        check_printf("%1f", neg_zero);

        check_printf("%2f", pos);
        check_printf("%2f", pos_int);
        check_printf("%2f", neg_int);
        check_printf("%2f", neg);
        check_printf("%2f", pos_zero);
        check_printf("%2f", neg_zero);

        check_printf("%5f", pos);
        check_printf("%5f", pos_int);
        check_printf("%5f", neg_int);
        check_printf("%5f", neg);
        check_printf("%5f", pos_zero);
        check_printf("%5f", neg_zero);

        check_printf("%10f", pos);
        check_printf("%10f", pos_int);
        check_printf("%10f", neg_int);
        check_printf("%10f", neg);
        check_printf("%10f", pos_zero);
        check_printf("%10f", neg_zero);

        check_printf("% 1f", pos);
        check_printf("% 1f", pos_int);
        check_printf("% 1f", neg_int);
        check_printf("% 1f", neg);
        check_printf("% 1f", pos_zero);
        check_printf("% 1f", neg_zero);

        check_printf("% 2f", pos);
        check_printf("% 2f", pos_int);
        check_printf("% 2f", neg_int);
        check_printf("% 2f", neg);
        check_printf("% 2f", pos_zero);
        check_printf("% 2f", neg_zero);

        check_printf("% 5f", pos);
        check_printf("% 5f", pos_int);
        check_printf("% 5f", neg_int);
        check_printf("% 5f", neg);
        check_printf("% 5f", pos_zero);
        check_printf("% 5f", neg_zero);

        check_printf("% 10f", pos);
        check_printf("% 10f", pos_int);
        check_printf("% 10f", neg_int);
        check_printf("% 10f", neg);
        check_printf("% 10f", pos_zero);
        check_printf("% 10f", neg_zero);

        check_printf("%+1f", pos);
        check_printf("%+1f", pos_int);
        check_printf("%+1f", neg_int);
        check_printf("%+1f", neg);
        check_printf("%+1f", pos_zero);
        check_printf("%+1f", neg_zero);

        check_printf("%+2f", pos);
        check_printf("%+2f", pos_int);
        check_printf("%+2f", neg_int);
        check_printf("%+2f", neg);
        check_printf("%+2f", pos_zero);
        check_printf("%+2f", neg_zero);

        check_printf("%+5f", pos);
        check_printf("%+5f", pos_int);
        check_printf("%+5f", neg_int);
        check_printf("%+5f", neg);
        check_printf("%+5f", pos_zero);
        check_printf("%+5f", neg_zero);

        check_printf("%+10f", pos);
        check_printf("%+10f", pos_int);
        check_printf("%+10f", neg_int);
        check_printf("%+10f", neg);
        check_printf("%+10f", pos_zero);
        check_printf("%+10f", neg_zero);

        check_printf("%+ 1f", pos);
        check_printf("%+ 1f", pos_int);
        check_printf("%+ 1f", neg_int);
        check_printf("%+ 1f", neg);
        check_printf("%+ 1f", pos_zero);
        check_printf("%+ 1f", neg_zero);

        check_printf("%+ 2f", pos);
        check_printf("%+ 2f", pos_int);
        check_printf("%+ 2f", neg_int);
        check_printf("%+ 2f", neg);
        check_printf("%+ 2f", pos_zero);
        check_printf("%+ 2f", neg_zero);

        check_printf("%+ 5f", pos);
        check_printf("%+ 5f", pos_int);
        check_printf("%+ 5f", neg_int);
        check_printf("%+ 5f", neg);
        check_printf("%+ 5f", pos_zero);
        check_printf("%+ 5f", neg_zero);

        check_printf("%+ 10f", pos);
        check_printf("%+ 10f", pos_int);
        check_printf("%+ 10f", neg_int);
        check_printf("%+ 10f", neg);
        check_printf("%+ 10f", pos_zero);
        check_printf("%+ 10f", neg_zero);

        check_printf("%01f", pos);
        check_printf("%01f", pos_int);
        check_printf("%01f", neg_int);
        check_printf("%01f", neg);
        check_printf("%01f", pos_zero);
        check_printf("%01f", neg_zero);

        check_printf("%02f", pos);
        check_printf("%02f", pos_int);
        check_printf("%02f", neg_int);
        check_printf("%02f", neg);
        check_printf("%02f", pos_zero);
        check_printf("%02f", neg_zero);

        check_printf("%05f", pos);
        check_printf("%05f", pos_int);
        check_printf("%05f", neg_int);
        check_printf("%05f", neg);
        check_printf("%05f", pos_zero);
        check_printf("%05f", neg_zero);

        check_printf("%010f", pos);
        check_printf("%010f", pos_int);
        check_printf("%010f", neg_int);
        check_printf("%010f", neg);
        check_printf("%010f", pos_zero);
        check_printf("%010f", neg_zero);
}
END_TEST

START_TEST(test_normal_left_align)
{
        check_printf("%-1f", pos);
        check_printf("%-1f", pos_int);
        check_printf("%-1f", neg_int);
        check_printf("%-1f", neg);
        check_printf("%-1f", pos_zero);
        check_printf("%-1f", neg_zero);
        check_printf("%-5f", pos);
        check_printf("%-5f", pos_int);
        check_printf("%-5f", neg_int);
        check_printf("%-5f", neg);
        check_printf("%-5f", pos_zero);
        check_printf("%-5f", neg_zero);
        check_printf("%-10f", pos);
        check_printf("%-10f", pos_int);
        check_printf("%-10f", neg_int);
        check_printf("%-10f", neg);
        check_printf("%-10f", pos_zero);
        check_printf("%-10f", neg_zero);

        check_printf("% -1f", pos);
        check_printf("% -1f", pos_int);
        check_printf("% -1f", neg_int);
        check_printf("% -1f", neg);
        check_printf("% -1f", pos_zero);
        check_printf("% -1f", neg_zero);
        check_printf("% -5f", pos);
        check_printf("% -5f", pos_int);
        check_printf("% -5f", neg_int);
        check_printf("% -5f", neg);
        check_printf("% -5f", pos_zero);
        check_printf("% -5f", neg_zero);
        check_printf("% -10f", pos);
        check_printf("% -10f", pos_int);
        check_printf("% -10f", neg_int);
        check_printf("% -10f", neg);
        check_printf("% -10f", pos_zero);
        check_printf("% -10f", neg_zero);
}
END_TEST

START_TEST(test_normal_field_width_precision)
{
        check_printf("%10.f", pos);
        check_printf("%10.f", pos_int);
        check_printf("%10.f", neg_int);
        check_printf("%10.f", neg);
        check_printf("%10.f", pos_zero);
        check_printf("%10.f", neg_zero);
        check_printf_manual("      24.1", "%10.1f", pos);
        check_printf("%10.1f", pos_int);
        check_printf("%10.1f", neg_int);
        check_printf_manual("     -24.1", "%10.1f", neg);
        check_printf("%10.1f", pos_zero);
        check_printf("%10.1f", neg_zero);
        check_printf("%10.15f", pos);
        check_printf("%10.15f", pos_int);
        check_printf("%10.15f", neg_int);
        check_printf("%10.15f", neg);
        check_printf("%10.15f", pos_zero);
        check_printf("%10.15f", neg_zero);

        check_printf("%010.f", pos);
        check_printf("%010.f", pos_int);
        check_printf("%010.f", neg_int);
        check_printf("%010.f", neg);
        check_printf("%010.f", pos_zero);
        check_printf("%010.f", neg_zero);
        check_printf_manual("00000024.1", "%010.1f", pos);
        check_printf("%010.1f", pos_int);
        check_printf("%010.1f", neg_int);
        check_printf_manual("-0000024.1", "%010.1f", neg);
        check_printf("%010.1f", pos_zero);
        check_printf("%010.1f", neg_zero);
        check_printf("%010.15f", pos);
        check_printf("%010.15f", pos_int);
        check_printf("%010.15f", neg_int);
        check_printf("%010.15f", neg);
        check_printf("%010.15f", pos_zero);
        check_printf("%010.15f", neg_zero);

        check_printf("% 10.f", pos);
        check_printf("% 10.f", pos_int);
        check_printf("% 10.f", neg_int);
        check_printf("% 10.f", neg);
        check_printf("% 10.f", pos_zero);
        check_printf("% 10.f", neg_zero);
        check_printf_manual("      24.1", "% 10.1f", pos);
        check_printf("% 10.1f", pos_int);
        check_printf("% 10.1f", neg_int);
        check_printf_manual("     -24.1", "% 10.1f", neg);
        check_printf("% 10.1f", pos_zero);
        check_printf("% 10.1f", neg_zero);
        check_printf("% 10.15f", pos);
        check_printf("% 10.15f", pos_int);
        check_printf("% 10.15f", neg_int);
        check_printf("% 10.15f", neg);
        check_printf("% 10.15f", pos_zero);
        check_printf("% 10.15f", neg_zero);

        check_printf("%+10.f", pos);
        check_printf("%+10.f", pos_int);
        check_printf("%+10.f", neg_int);
        check_printf("%+10.f", neg);
        check_printf("%+10.f", pos_zero);
        check_printf("%+10.f", neg_zero);
        check_printf_manual("     +24.1", "%+10.1f", pos);
        check_printf("%+10.1f", pos_int);
        check_printf("%+10.1f", neg_int);
        check_printf_manual("     -24.1", "%+10.1f", neg);
        check_printf("%+10.1f", pos_zero);
        check_printf("%+10.1f", neg_zero);
        check_printf("%+10.15f", pos);
        check_printf("%+10.15f", pos_int);
        check_printf("%+10.15f", neg_int);
        check_printf("%+10.15f", neg);
        check_printf("%+10.15f", pos_zero);
        check_printf("%+10.15f", neg_zero);
}
END_TEST

START_TEST(test_normal_left_align_precision)
{
        check_printf("%-10.f", pos);
        check_printf("%-10.f", pos_int);
        check_printf("%-10.f", neg_int);
        check_printf("%-10.f", neg);
        check_printf("%-10.f", pos_zero);
        check_printf("%-10.f", neg_zero);
        check_printf_manual("24.1      ", "%-10.1f", pos);
        check_printf("%-10.1f", pos_int);
        check_printf("%-10.1f", neg_int);
        check_printf_manual("-24.1     ", "%-10.1f", neg);
        check_printf("%-10.1f", pos_zero);
        check_printf("%-10.1f", neg_zero);
        check_printf("%-10.15f", pos);
        check_printf("%-10.15f", pos_int);
        check_printf("%-10.15f", neg_int);
        check_printf("%-10.15f", neg);
        check_printf("%-10.15f", pos_zero);
        check_printf("%-10.15f", neg_zero);

        check_printf("%- 10.f", pos);
        check_printf("%- 10.f", pos_int);
        check_printf("%- 10.f", neg_int);
        check_printf("%- 10.f", neg);
        check_printf("%- 10.f", pos_zero);
        check_printf("%- 10.f", neg_zero);
        check_printf_manual(" 24.1     ", "%- 10.1f", pos);
        check_printf("%- 10.1f", pos_int);
        check_printf("%- 10.1f", neg_int);
        check_printf_manual("-24.1     ", "%- 10.1f", neg);
        check_printf("%- 10.1f", pos_zero);
        check_printf("%- 10.1f", neg_zero);
        check_printf("%- 10.15f", pos);
        check_printf("%- 10.15f", pos_int);
        check_printf("%- 10.15f", neg_int);
        check_printf("%- 10.15f", neg);
        check_printf("%- 10.15f", pos_zero);
        check_printf("%- 10.15f", neg_zero);

        check_printf("%-+10.f", pos);
        check_printf("%-+10.f", pos_int);
        check_printf("%-+10.f", neg_int);
        check_printf("%-+10.f", neg);
        check_printf("%-+10.f", pos_zero);
        check_printf("%-+10.f", neg_zero);
        check_printf_manual("+24.1     ", "%-+10.1f", pos);
        check_printf("%-+10.1f", pos_int);
        check_printf("%-+10.1f", neg_int);
        check_printf_manual("-24.1     ", "%-+10.1f", neg);
        check_printf("%-+10.1f", pos_zero);
        check_printf("%-+10.1f", neg_zero);
        check_printf("%-+10.15f", pos);
        check_printf("%-+10.15f", pos_int);
        check_printf("%-+10.15f", neg_int);
        check_printf("%-+10.15f", neg);
        check_printf("%-+10.15f", pos_zero);
        check_printf("%-+10.15f", neg_zero);

}
END_TEST

START_TEST(test_exp_conversion)
{
        check_printf("%e", pos);
        check_printf("%e", pos_int);
        check_printf("%e", neg_int);
        check_printf("%e", neg);
        check_printf("%e", pos_zero);
        check_printf("%e", neg_zero);

        check_printf("%E", pos);
        check_printf("%E", pos_int);
        check_printf("%E", neg_int);
        check_printf("%E", neg);
        check_printf("%E", pos_zero);
        check_printf("%E", neg_zero);
}
END_TEST

START_TEST(test_exp_show_sign)
{
        check_printf("%+e", pos);
        check_printf("%+e", pos_int);
        check_printf("%+e", neg_int);
        check_printf("%+e", neg);
        check_printf("%+e", pos_zero);
        check_printf("%+e", neg_zero);
        check_printf("%+E", pos);
        check_printf("%+E", pos_int);
        check_printf("%+E", neg_int);
        check_printf("%+E", neg);
        check_printf("%+E", pos_zero);
        check_printf("%+E", neg_zero);

        check_printf("% +e", pos);
        check_printf("% +e", pos_int);
        check_printf("% +e", neg_int);
        check_printf("% +e", neg);
        check_printf("% +e", pos_zero);
        check_printf("% +e", neg_zero);
        check_printf("% +E", pos);
        check_printf("% +E", pos_int);
        check_printf("% +E", neg_int);
        check_printf("% +E", neg);
        check_printf("% +E", pos_zero);
        check_printf("% +E", neg_zero);

}
END_TEST

START_TEST(test_exp_initial_space)
{
        check_printf("% e", pos);
        check_printf("% e", pos_int);
        check_printf("% e", neg_int);
        check_printf("% e", neg);
        check_printf("% e", pos_zero);
        check_printf("% e", neg_zero);

        check_printf("% E", pos);
        check_printf("% E", pos_int);
        check_printf("% E", neg_int);
        check_printf("% E", neg);
        check_printf("% E", pos_zero);
        check_printf("% E", neg_zero);
}
END_TEST

START_TEST(test_exp_alternative_output)
{
        check_printf("%#e", pos);
        check_printf("%#e", pos_int);
        check_printf("%#e", neg_int);
        check_printf("%#e", neg);
        check_printf("%#e", pos_zero);
        check_printf("%#e", neg_zero);
        check_printf("%#E", pos);
        check_printf("%#E", pos_int);
        check_printf("%#E", neg_int);
        check_printf("%#E", neg);
        check_printf("%#E", pos_zero);
        check_printf("%#E", neg_zero);

        check_printf("%#.e", pos);
        check_printf("%#.e", pos_int);
        check_printf("%#.e", neg_int);
        check_printf("%#.e", neg);
        check_printf("%#.e", pos_zero);
        check_printf("%#.e", neg_zero);
        check_printf("%#.E", pos);
        check_printf("%#.E", pos_int);
        check_printf("%#.E", neg_int);
        check_printf("%#.E", neg);
        check_printf("%#.E", pos_zero);
        check_printf("%#.E", neg_zero);
}
END_TEST

START_TEST(test_exp_precision)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        /*
         * I can't compare to local "printf" implementation because my
         * implementation doesn't do rounding.
         */
        check_printf("%.e", pos);
        check_printf("%.e", neg);
        check_printf("%.e", fp_frac);
        check_printf("%.e", fp_int);
        check_printf("%.e", big);
        check_printf("%.e", neg_big);
        check_printf("%.e", nine);
        check_printf("%.e", bbig);
        check_printf("%.e", nbbig);

        check_printf("%.1e", pos);
        check_printf("%.1e", neg);
        check_printf("%.1e", fp_frac);
        check_printf_manual("2.4e+03", "%.1e", fp_int);
        check_printf("%.1e", big);
        check_printf("%.1e", neg_big);
        check_printf("%.1e", nine);
        check_printf("%.1e", bbig);
        check_printf_manual("-3.2e+11", "%.1e", nbbig);

        check_printf("%.2e", pos);
        check_printf("%.2e", neg);
        check_printf("%.2e", fp_frac);
        check_printf("%.2e", fp_int);
        check_printf("%.2e", big);
        check_printf("%.2e", neg_big);
        check_printf("%.2e", nine);
        check_printf("%.2e", bbig);
        check_printf("%.2e", nbbig);

        check_printf("%.5e", pos);
        check_printf("%.5e", neg);
        check_printf_manual("3.34322e-01", "%.5e", fp_frac);
        check_printf_manual("2.46399e+03", "%.5e", fp_int);
        check_printf("%.5e", big);
        check_printf("%.5e", neg_big);
        check_printf_manual("2.32399e+01", "%.5e", nine);
        check_printf("%.5e", bbig);
        check_printf("%.5e", nbbig);

        check_printf_manual("2.4432344999e+03", "%.10e", pos);
        check_printf_manual("-2.3432234209e+04", "%.10e", neg);
        check_printf("%.10e", fp_frac);
        check_printf_manual("2.4639999999e+03", "%.10e", fp_int);
        check_printf_manual("2.2343532399e+08", "%.10e", big);
        check_printf_manual("-4.3244521999e+07", "%.10e", neg_big);
        check_printf_manual("2.3239999999e+01", "%.10e", nine);
        check_printf("%.10e", bbig);
        check_printf_manual("-3.2543326999e+11", "%.10e", nbbig);

        check_printf("%.E", pos);
        check_printf("%.E", neg);
        check_printf("%.E", fp_frac);
        check_printf("%.E", fp_int);
        check_printf("%.E", big);
        check_printf("%.E", neg_big);
        check_printf("%.E", nine);
        check_printf("%.E", bbig);
        check_printf("%.E", nbbig);

        check_printf("%.1E", pos);
        check_printf("%.1E", neg);
        check_printf("%.1E", fp_frac);
        check_printf_manual("2.4E+03", "%.1E", fp_int);
        check_printf("%.1E", big);
        check_printf("%.1E", neg_big);
        check_printf("%.1E", nine);
        check_printf("%.1E", bbig);
        check_printf_manual("-3.2E+11", "%.1E", nbbig);

        check_printf("%.2E", pos);
        check_printf("%.2E", neg);
        check_printf("%.2E", fp_frac);
        check_printf("%.2E", fp_int);
        check_printf("%.2E", big);
        check_printf("%.2E", neg_big);
        check_printf("%.2E", nine);
        check_printf("%.2E", bbig);
        check_printf("%.2E", nbbig);

        check_printf("%.5E", pos);
        check_printf("%.5E", neg);
        check_printf_manual("3.34322E-01", "%.5E", fp_frac);
        check_printf_manual("2.46399E+03", "%.5E", fp_int);
        check_printf("%.5E", big);
        check_printf("%.5E", neg_big);
        check_printf_manual("2.32399E+01", "%.5E", nine);
        check_printf("%.5E", bbig);
        check_printf("%.5E", nbbig);

        check_printf_manual("2.4432344999E+03", "%.10E", pos);
        check_printf_manual("-2.3432234209E+04", "%.10E", neg);
        check_printf("%.10E", fp_frac);
        check_printf_manual("2.4639999999E+03", "%.10E", fp_int);
        check_printf_manual("2.2343532399E+08", "%.10E", big);
        check_printf_manual("-4.3244521999E+07", "%.10E", neg_big);
        check_printf_manual("2.3239999999E+01", "%.10E", nine);
        check_printf("%.10E", bbig);
        check_printf_manual("-3.2543326999E+11", "%.10E", nbbig);
}
END_TEST

START_TEST(test_exp_field_width)
{
        check_printf("%1e", pos);
        check_printf("%1e", pos_int);
        check_printf("%1e", neg_int);
        check_printf("%1e", neg);
        check_printf("%1e", pos_zero);
        check_printf("%1e", neg_zero);

        check_printf("%5e", pos);
        check_printf("%5e", pos_int);
        check_printf("%5e", neg_int);
        check_printf("%5e", neg);
        check_printf("%5e", pos_zero);
        check_printf("%5e", neg_zero);

        check_printf("%10e", pos);
        check_printf("%10e", pos_int);
        check_printf("%10e", neg_int);
        check_printf("%10e", neg);
        check_printf("%10e", pos_zero);
        check_printf("%10e", neg_zero);

        check_printf("%20e", pos);
        check_printf("%20e", pos_int);
        check_printf("%20e", neg_int);
        check_printf("%20e", neg);
        check_printf("%20e", pos_zero);
        check_printf("%20e", neg_zero);

        check_printf("%+1e", pos);
        check_printf("%+1e", pos_int);
        check_printf("%+1e", neg_int);
        check_printf("%+1e", neg);
        check_printf("%+1e", pos_zero);
        check_printf("%+1e", neg_zero);

        check_printf("%+5e", pos);
        check_printf("%+5e", pos_int);
        check_printf("%+5e", neg_int);
        check_printf("%+5e", neg);
        check_printf("%+5e", pos_zero);
        check_printf("%+5e", neg_zero);

        check_printf("%+10e", pos);
        check_printf("%+10e", pos_int);
        check_printf("%+10e", neg_int);
        check_printf("%+10e", neg);
        check_printf("%+10e", pos_zero);
        check_printf("%+10e", neg_zero);

        check_printf("%+20e", pos);
        check_printf("%+20e", pos_int);
        check_printf("%+20e", neg_int);
        check_printf("%+20e", neg);
        check_printf("%+20e", pos_zero);
        check_printf("%+20e", neg_zero);

        check_printf("% 1e", pos);
        check_printf("% 1e", pos_int);
        check_printf("% 1e", neg_int);
        check_printf("% 1e", neg);
        check_printf("% 1e", pos_zero);
        check_printf("% 1e", neg_zero);

        check_printf("% 5e", pos);
        check_printf("% 5e", pos_int);
        check_printf("% 5e", neg_int);
        check_printf("% 5e", neg);
        check_printf("% 5e", pos_zero);
        check_printf("% 5e", neg_zero);

        check_printf("% 10e", pos);
        check_printf("% 10e", pos_int);
        check_printf("% 10e", neg_int);
        check_printf("% 10e", neg);
        check_printf("% 10e", pos_zero);
        check_printf("% 10e", neg_zero);

        check_printf("% 20e", pos);
        check_printf("% 20e", pos_int);
        check_printf("% 20e", neg_int);
        check_printf("% 20e", neg);
        check_printf("% 20e", pos_zero);
        check_printf("% 20e", neg_zero);

        check_printf("%01e", pos);
        check_printf("%01e", pos_int);
        check_printf("%01e", neg_int);
        check_printf("%01e", neg);
        check_printf("%01e", pos_zero);
        check_printf("%01e", neg_zero);

        check_printf("%05e", pos);
        check_printf("%05e", pos_int);
        check_printf("%05e", neg_int);
        check_printf("%05e", neg);
        check_printf("%05e", pos_zero);
        check_printf("%05e", neg_zero);

        check_printf("%010e", pos);
        check_printf("%010e", pos_int);
        check_printf("%010e", neg_int);
        check_printf("%010e", neg);
        check_printf("%010e", pos_zero);
        check_printf("%010e", neg_zero);

        check_printf("%020e", pos);
        check_printf("%020e", pos_int);
        check_printf("%020e", neg_int);
        check_printf("%020e", neg);
        check_printf("%020e", pos_zero);
        check_printf("%020e", neg_zero);
}
END_TEST

START_TEST(test_exp_left_align)
{
        check_printf("%-10e", pos);
        check_printf("%-10e", pos_int);
        check_printf("%-10e", neg_int);
        check_printf("%-10e", neg);
        check_printf("%-10e", pos_zero);
        check_printf("%-10e", neg_zero);

        check_printf("%-15e", pos);
        check_printf("%-15e", pos_int);
        check_printf("%-15e", neg_int);
        check_printf("%-15e", neg);
        check_printf("%-15e", pos_zero);
        check_printf("%-15e", neg_zero);

        check_printf("%-20e", pos);
        check_printf("%-20e", pos_int);
        check_printf("%-20e", neg_int);
        check_printf("%-20e", neg);
        check_printf("%-20e", pos_zero);
        check_printf("%-20e", neg_zero);

        check_printf("% -20e", pos);
        check_printf("% -20e", pos_int);
        check_printf("% -20e", neg_int);
        check_printf("% -20e", neg);
        check_printf("% -20e", pos_zero);
        check_printf("% -20e", neg_zero);

        check_printf("%+-20e", pos);
        check_printf("%+-20e", pos_int);
        check_printf("%+-20e", neg_int);
        check_printf("%+-20e", neg);
        check_printf("%+-20e", pos_zero);
        check_printf("%+-20e", neg_zero);

        check_printf("%0-20e", pos);
        check_printf("%0-20e", pos_int);
        check_printf("%0-20e", neg_int);
        check_printf("%0-20e", neg);
        check_printf("%0-20e", pos_zero);
        check_printf("%0-20e", neg_zero);
}
END_TEST

START_TEST(test_exp_left_align_precision)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        check_printf("%-15.e", pos);
        check_printf("%-15.e", neg);
        check_printf("%-15.e", fp_frac);
        check_printf("%-15.e", fp_int);
        check_printf("%-15.e", big);
        check_printf("%-15.e", neg_big);
        check_printf("%-15.e", nine);
        check_printf("%-15.e", bbig);
        check_printf("%-15.e", nbbig);

        check_printf("%-15.5e", pos);
        check_printf("%-15.5e", neg);
        check_printf_manual("3.34322e-01    ", "%-15.5e", fp_frac);
        check_printf_manual("2.46399e+03    ", "%-15.5e", fp_int);
        check_printf("%-15.5e", big);
        check_printf("%-15.5e", neg_big);
        check_printf_manual("2.32399e+01    ", "%-15.5e", nine);
        check_printf("%-15.5e", bbig);
        check_printf("%-15.5e", nbbig);
}
END_TEST

START_TEST(test_exp_field_width_precision)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        check_printf("%15.e", pos);
        check_printf("%15.e", neg);
        check_printf("%15.e", fp_frac);
        check_printf("%15.e", fp_int);
        check_printf("%15.e", big);
        check_printf("%15.e", neg_big);
        check_printf("%15.e", nine);
        check_printf("%15.e", bbig);
        check_printf("%15.e", nbbig);

        check_printf("%15.5e", pos);
        check_printf("%15.5e", neg);
        check_printf_manual("    3.34322e-01", "%15.5e", fp_frac);
        check_printf_manual("    2.46399e+03", "%15.5e", fp_int);
        check_printf("%15.5e", big);
        check_printf("%15.5e", neg_big);
        check_printf_manual("    2.32399e+01", "%15.5e", nine);
        check_printf("%15.5e", bbig);
        check_printf("%15.5e", nbbig);

        check_printf("%015.5e", pos);
        check_printf("%015.5e", neg);
        check_printf_manual("00003.34322e-01", "%015.5e", fp_frac);
        check_printf_manual("00002.46399e+03", "%015.5e", fp_int);
        check_printf("%015.5e", big);
        check_printf("%015.5e", neg_big);
        check_printf_manual("00002.32399e+01", "%015.5e", nine);
        check_printf("%015.5e", bbig);
        check_printf("%015.5e", nbbig);

}
END_TEST

START_TEST(test_inf)
{
        check_printf("%f", pos_inf);
        check_printf("%f", neg_inf);

        check_printf("% f", pos_inf);
        check_printf("% f", neg_inf);

        /* Zero pad should be ignored. */
        check_printf("%0f", pos_inf);
        check_printf("%0f", neg_inf);

        /* Alternative output should be ignored. */
        check_printf("%#f", pos_inf);
        check_printf("%#f", neg_inf);

        /* Zero pad and initial space should be ignored in favour of sign. */
        check_printf("%+f", pos_inf);
        check_printf("%+f", neg_inf);
        check_printf("%0+f", pos_inf);
        check_printf("%0+f", neg_inf);
        check_printf("% +f", pos_inf);
        check_printf("% +f", neg_inf);
        check_printf("%0 +f", pos_inf);
        check_printf("%0 +f", neg_inf);

        /* Precision should be ignored. */
        check_printf("%.f", pos_inf);
        check_printf("%.f", neg_inf);
        check_printf("%.1f", pos_inf);
        check_printf("%.1f", neg_inf);
        check_printf("%.3f", pos_inf);
        check_printf("%.3f", neg_inf);
        check_printf("%.5f", pos_inf);
        check_printf("%.5f", neg_inf);

        check_printf("%1f", pos_inf);
        check_printf("%1f", neg_inf);
        check_printf("%2f", pos_inf);
        check_printf("%2f", neg_inf);
        check_printf("%3f", pos_inf);
        check_printf("%3f", neg_inf);
        check_printf("%4f", pos_inf);
        check_printf("%4f", neg_inf);
        check_printf("%5f", pos_inf);
        check_printf("%5f", neg_inf);

        /* Initial space or sign must be consider as part of left padding. */
        check_printf("% 1f", pos_inf);
        check_printf("% 1f", neg_inf);
        check_printf("% 2f", pos_inf);
        check_printf("% 2f", neg_inf);
        check_printf("% 3f", pos_inf);
        check_printf("% 3f", neg_inf);
        check_printf("% 4f", pos_inf);
        check_printf("% 4f", neg_inf);
        check_printf("% 5f", pos_inf);
        check_printf("% 5f", neg_inf);

        check_printf("%+1f", pos_inf);
        check_printf("%+1f", neg_inf);
        check_printf("%+2f", pos_inf);
        check_printf("%+2f", neg_inf);
        check_printf("%+3f", pos_inf);
        check_printf("%+3f", neg_inf);
        check_printf("%+4f", pos_inf);
        check_printf("%+4f", neg_inf);
        check_printf("%+5f", pos_inf);
        check_printf("%+5f", neg_inf);

        check_printf("%-1f", pos_inf);
        check_printf("%-1f", neg_inf);
        check_printf("%-2f", pos_inf);
        check_printf("%-2f", neg_inf);
        check_printf("%-3f", pos_inf);
        check_printf("%-3f", neg_inf);
        check_printf("%-4f", pos_inf);
        check_printf("%-4f", neg_inf);
        check_printf("%-5f", pos_inf);
        check_printf("%-5f", neg_inf);

        /*
         * Initial space or sign must not be considered as part of right padding
         */
        check_printf("%-+1f", pos_inf);
        check_printf("%-+1f", neg_inf);
        check_printf("%-+2f", pos_inf);
        check_printf("%-+2f", neg_inf);
        check_printf("%-+3f", pos_inf);
        check_printf("%-+3f", neg_inf);
        check_printf("%-+4f", pos_inf);
        check_printf("%-+4f", neg_inf);
        check_printf("%-+5f", pos_inf);
        check_printf("%-+5f", neg_inf);

        check_printf("%- 1f", pos_inf);
        check_printf("%- 1f", neg_inf);
        check_printf("%- 2f", pos_inf);
        check_printf("%- 2f", neg_inf);
        check_printf("%- 3f", pos_inf);
        check_printf("%- 3f", neg_inf);
        check_printf("%- 4f", pos_inf);
        check_printf("%- 4f", neg_inf);
        check_printf("%- 5f", pos_inf);
        check_printf("%- 5f", neg_inf);

        /* 'f', 'g', 'e' are the same for "inf", except for uppercase. */
        check_printf("%e", pos_inf);
        check_printf("%e", neg_inf);
        check_printf("%g", pos_inf);
        check_printf("%g", neg_inf);
        check_printf("%E", pos_inf);
        check_printf("%E", neg_inf);
        check_printf("%G", pos_inf);
        check_printf("%G", neg_inf);
}
END_TEST

START_TEST(test_nan)
{
        /*
         * In C89 I can't generate NAN, I don't even know if there is NAN
         * support. C99 instead has useful macros and functions, so for now
         * floating point numbers are not fully supported in my implementation.
         *
         * This is an hack to generate NAN, but it is not guaranteed to work on
         * all platforms.
         */
        const double nan = 0. / 0.;

        /*
         * I do not consider sign bit for NAN, but glibc's printf consider it.
         */
        check_printf_manual("nan", "%f", nan);
        check_printf_manual(" nan", "% f", nan);

        /* Most flags are ignored with NAN. */
        check_printf_manual("nan", "%#f", nan);
        check_printf_manual("nan", "%+f", nan);
        check_printf_manual("nan", "%0f", nan);
        check_printf_manual("nan", "%.1f", nan);
        check_printf_manual("nan", "%.3f", nan);
        check_printf_manual("nan", "%.4f", nan);
        check_printf_manual("nan", "%.5f", nan);

        check_printf_manual("nan", "%1f", nan);
        check_printf_manual("nan", "%2f", nan);
        check_printf_manual("nan", "%3f", nan);
        check_printf_manual(" nan", "%4f", nan);
        check_printf_manual("  nan", "%5f", nan);

        check_printf_manual(" nan", "% 1f", nan);
        check_printf_manual(" nan", "% 2f", nan);
        check_printf_manual(" nan", "% 3f", nan);
        check_printf_manual(" nan", "% 4f", nan);
        check_printf_manual("  nan", "% 5f", nan);

        check_printf_manual("nan", "%-1f", nan);
        check_printf_manual("nan", "%-2f", nan);
        check_printf_manual("nan", "%-3f", nan);
        check_printf_manual("nan ", "%-4f", nan);
        check_printf_manual("nan  ", "%-5f", nan);

        check_printf_manual(" nan", "% -1f", nan);
        check_printf_manual(" nan", "% -2f", nan);
        check_printf_manual(" nan", "% -3f", nan);
        check_printf_manual(" nan", "% -4f", nan);
        check_printf_manual(" nan ", "% -5f", nan);

        /* 'f', 'g', 'e' are the same for NAN, except for uppercase. */
        check_printf_manual("nan", "%f", nan);
        check_printf_manual("nan", "%e", nan);
        check_printf_manual("nan", "%g", nan);
        check_printf_manual("NAN", "%E", nan);
        check_printf_manual("NAN", "%G", nan);
}
END_TEST

/* "%g" or "%G" specifiers. */
START_TEST(test_mix_conversion)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        check_printf("%g", pos);
        check_printf("%g", neg);
        check_printf_manual("0.334322", "%g", fp_frac);
        check_printf("%g", fp_int);
        check_printf("%g", big);
        check_printf("%g", neg_big);
        check_printf_manual("23.2399", "%g", nine);
        check_printf("%g", bbig);
        check_printf("%g", nbbig);

        check_printf("%G", pos);
        check_printf("%G", neg);
        check_printf_manual("0.334322", "%G", fp_frac);
        check_printf("%G", fp_int);
        check_printf("%G", big);
        check_printf("%G", neg_big);
        check_printf_manual("23.2399", "%G", nine);
        check_printf("%G", bbig);
        check_printf("%G", nbbig);

        check_printf("%g", 24.000);
        check_printf("%g", 24.000000);
        check_printf_manual("2.33199e-05", "%g", 0.00002332);
}
END_TEST

START_TEST(test_mix_precision)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        check_printf("%.g", pos);
        check_printf("%.g", neg);
        check_printf("%.g", fp_frac);
        check_printf("%.g", fp_int);
        check_printf("%.g", big);
        check_printf("%.g", neg_big);
        check_printf("%.g", nine);
        check_printf("%.g", bbig);
        check_printf("%.g", nbbig);

        check_printf("%.3g", pos);
        check_printf("%.3g", neg);
        check_printf("%.3g", fp_frac);
        check_printf("%.3g", fp_int);
        check_printf("%.3g", big);
        check_printf("%.3g", neg_big);
        check_printf("%.3g", nine);
        check_printf("%.3g", bbig);
        check_printf("%.3g", nbbig);

        check_printf("%.15g", pos);
        check_printf_manual("-23432.2342", "%.15g", neg);
        check_printf("%.15g", fp_frac);
        check_printf("%.15g", fp_int);
        check_printf("%.15g", big);
        check_printf("%.15g", neg_big);
        check_printf("%.15g", nine);
        check_printf("%.15g", bbig);
        check_printf("%.15g", nbbig);
}
END_TEST

START_TEST(test_mix_alternative_output)
{
        double pos = 2443.2345, neg = -23432.23421, fp_frac = 0.334322532;
        double fp_int = 2464, big = 223435324, neg_big = -43244522;
        double nine = 23.23999999999, bbig = 23324435332232;
        double nbbig = -325433269999;

        check_printf("%#g", pos);
        check_printf("%#g", neg);
        check_printf_manual("0.334322", "%#g", fp_frac);
        check_printf("%#g", fp_int);
        check_printf("%#g", big);
        check_printf("%#g", neg_big);
        check_printf_manual("23.2399", "%#g", nine);
        check_printf("%#g", bbig);
        check_printf("%#g", nbbig);

        check_printf("%#.g", pos);
        check_printf("%#.g", neg);
        check_printf("%#.g", fp_frac);
        check_printf("%#.g", fp_int);
        check_printf("%#.g", big);
        check_printf("%#.g", neg_big);
        check_printf("%#.g", nine);
        check_printf("%#.g", bbig);
        check_printf("%#.g", nbbig);

        check_printf("%#.15g", pos);
        check_printf_manual("-23432.2342099999", "%#.15g", neg);
        check_printf("%#.15g", fp_frac);
        check_printf("%#.15g", fp_int);
        check_printf("%#.15g", big);
        check_printf("%#.15g", neg_big);
        check_printf("%#.15g", nine);
        check_printf("%#.15g", bbig);
        check_printf("%#.15g", nbbig);
}
END_TEST

Suite *make_e_printf_float_suite(void)
{
        Suite *suite = suite_create("e_printf_float");
        TCase *test_normal = tcase_create("e_printf_float_normal");
        TCase *test_exp = tcase_create("e_printf_float_exp");
        TCase *test_special = tcase_create("e_printf_float_special");
        TCase *test_mix = tcase_create("e_printf_float_mix");

        tcase_add_test(test_normal, test_normal_conversion);
        tcase_add_test(test_normal, test_normal_show_sign);
        tcase_add_test(test_normal, test_normal_initial_space);
        tcase_add_test(test_normal, test_normal_alternative_output);
        tcase_add_test(test_normal, test_normal_precision);
        tcase_add_test(test_normal, test_normal_field_width);
        tcase_add_test(test_normal, test_normal_left_align);
        tcase_add_test(test_normal, test_normal_left_align_precision);
        tcase_add_test(test_normal, test_normal_field_width_precision);

        tcase_add_test(test_exp, test_exp_conversion);
        tcase_add_test(test_exp, test_exp_show_sign);
        tcase_add_test(test_exp, test_exp_initial_space);
        tcase_add_test(test_exp, test_exp_alternative_output);
        tcase_add_test(test_exp, test_exp_precision);
        tcase_add_test(test_exp, test_exp_field_width);
        tcase_add_test(test_exp, test_exp_left_align);
        tcase_add_test(test_exp, test_exp_left_align_precision);
        tcase_add_test(test_exp, test_exp_field_width_precision);

        tcase_add_test(test_special, test_inf);
        tcase_add_test(test_special, test_nan);

        tcase_add_test(test_mix, test_mix_conversion);
        tcase_add_test(test_mix, test_mix_precision);
        tcase_add_test(test_mix, test_mix_alternative_output);

        suite_add_tcase(suite, test_normal);
        suite_add_tcase(suite, test_exp);
        suite_add_tcase(suite, test_special);
        suite_add_tcase(suite, test_mix);

        return suite;
}
