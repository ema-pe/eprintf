/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include <check.h>

#include "../src/eprintf.h"

#include "check_eprintf.h"

/*
 * check_printf_integer: it is like "check_printf" but it tests for "%n"
 * argument.
 *
 * Parameters:
 *      - n: a pointer to the same integer pointer passed in "...";
 *      - format: a format string that contains "%n";
 *      - ...: arguments for "format".
 */
static void check_printf_integer(int *n, char *format, ...)
{
        static const char *fmt = "Failed: format(\"%s\") = \"%s\" (%d) "
                                 "expected \"%s\" (%d)";
        char my[CHECK_BUF_SIZE], std[CHECK_BUF_SIZE];
        va_list ap_my, ap_std;
        int my_n, std_n;

        assert(n != NULL && format != NULL);

        va_start(ap_my, format);
        va_start(ap_std, format);

        e_vsprintf(my, format, ap_my);
        my_n = *n;

        vsprintf(std, format, ap_std);
        std_n = *n;

        ck_assert_msg(strcmp(my, std) == 0 && my_n == std_n, fmt, format, my,
                      my_n, std, std_n);

        va_end(ap_my);
        va_end(ap_std);
}

START_TEST(test_save_integer)
{
        int n;

        check_printf_integer(&n, "hello world %n", &n);
        check_printf_integer(&n, "%n", &n);

        check_printf_integer(&n, "%50d%n", 245, &n);
        check_printf_integer(&n, "%50f%n", 245.299, &n);
        check_printf_integer(&n, "%n%n", &n, &n);
}
END_TEST

START_TEST(test_literal_percent)
{
        check_printf("%%");
        check_printf("%");
}
END_TEST

START_TEST(test_star)
{
        int pos_int = 3532;
        double pos_flt = 4364.2353;

        check_printf("%*d", 10, pos_int);
        check_printf("%*d", 0, pos_int);
        check_printf("%*d", -10, pos_int);

        check_printf("%.*d", 10, pos_int);
        check_printf("%.*d", 0, pos_int);
        check_printf("%.*d", -10, pos_int);

        check_printf("%*f", 15, pos_flt);
        check_printf("%*f", 0, pos_flt);
        check_printf("%*f", -15, pos_flt);

        check_printf("%.*f", 15, pos_flt);
        check_printf("%.*f", 0, pos_flt);
        check_printf("%.*f", -15, pos_flt);

        check_printf("%.*f", INT_MIN, pos_flt);
}
END_TEST

START_TEST(test_pointer)
{
        int p1 = 2422;
        double p2 = 2343.43322;
        char c = '4';

        check_printf("%p", &p1);
        check_printf("%p", &p2);
        check_printf("%p", &c);
}
END_TEST

Suite *make_e_printf_special_suite(void)
{
        Suite *suite = suite_create("e_printf_special");
        TCase *test = tcase_create("e_printf_special");

        tcase_add_test(test, test_save_integer);
        tcase_add_test(test, test_literal_percent);
        tcase_add_test(test, test_star);

        if (sizeof(void *) <= sizeof(unsigned long))
                tcase_add_test(test, test_pointer);

        suite_add_tcase(suite, test);
        return suite;
}
