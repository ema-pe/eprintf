/*
 * Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
 *
 * All rights reserved. This file is licensed under the MIT license. See LICENSE
 * file for more information.
 */
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

#include <check.h>

#include "../src/eprintf.h"

#include "check_eprintf.h"

static const char *fmt = "Failed: format(\"%s\") = \"%s\" (%d) "
                         "expected \"%s\" (%d)";

void check_printf(char *format, ...)
{
        char my[CHECK_BUF_SIZE], std[CHECK_BUF_SIZE];
        int my_retval, std_retval;
        va_list ap_my, ap_std;

        assert(format != NULL);

        va_start(ap_my, format);
        va_start(ap_std, format);

        my_retval = e_vsprintf(my, format, ap_my);
        std_retval = vsprintf(std, format, ap_std);

        ck_assert_msg(strcmp(my, std) == 0 && my_retval == std_retval, fmt,
                      format, my, my_retval, std, std_retval);

        va_end(ap_my);
        va_end(ap_std);
}

void check_printf_manual(char *expected, char *format, ...)
{
        char my[CHECK_BUF_SIZE];
        int my_retval, expected_length;
        va_list ap;

        assert(expected != NULL && format != NULL);

        va_start(ap, format);

        my_retval = e_vsprintf(my, format, ap);
        expected_length = strlen(expected);

        ck_assert_msg(strcmp(my, expected) == 0 && my_retval == expected_length,
                      fmt, format, my, my_retval, expected, expected_length);

        va_end(ap);
}

int main(void)
{
        int tests_failed;
        SRunner *runner;

        runner = srunner_create(make_e_printf_integer_suite());
        srunner_add_suite(runner, make_e_printf_str_suite());
        srunner_add_suite(runner, make_e_printf_char_suite());
        srunner_add_suite(runner, make_e_printf_float_suite());
        srunner_add_suite(runner, make_e_printf_special_suite());

        srunner_run_all(runner, CK_NORMAL);

        tests_failed = srunner_ntests_failed(runner);

        srunner_free(runner);

        return tests_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
