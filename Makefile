# Copyright © 2020 Emanuele Petriglia <inbox@emanuelepetriglia.com>
#
# All rights reserved. This file is licensed under the MIT license. See LICENSE
# file for more information.
#
# I'm new to Makefile and I've read GNU Make manual to learn how to write this
# file, so it can have issues or errors!
SHELL = /bin/sh

.SUFFIXES:

.PHONY = all install clean check

CC = gcc

CFLAGS += -Wpedantic -Wall -Wextra -ansi -g

DESTDIR ?=

prefix = $(DESTDIR)/usr/local
includedir = $(prefix)/include
libdir = $(prefix)/lib
datarootdir = $(prefix)/share

tests_srcs = $(wildcard tests/*.c)
tests_objs = $(patsubst %.c,%.o,$(tests_srcs))

aux = README.adoc LICENSE

all: check libeprintf.so

install : $(header) libeprintf.so
	install -D src/eprintf.h $(includedir)/eprintf.h
	install -D libeprintf.so $(libdir)/libeprintf.so
	install -D README.adoc $(datarootdir)/eprintf/README.adoc
	install -D LICENSE $(datarootdir)/eprintf/LICENSE

check : run_tests
	./run_tests

clean :
	rm -f libeprintf.so libeprintf.a src/*.o tests/*.o run_tests

libeprintf.so : src/eprintf.o
	$(CC) -shared -o $@ $<

libeprintf.a : src/eprintf.o
	$(AR) rcs $@ $<

src/eprintf.o : src/eprintf.c src/eprintf.h
	$(CC) -c $(CFLAGS) -fPIC -o $@ $<

tests/%.o : tests/%.c tests/check_eprintf.h src/eprintf.h src/eprintf.c
	$(CC) -c $(CFLAGS) -o $@ $<

run_tests : $(tests_objs) src/eprintf.o
	$(CC) $(CFLAGS) -lcheck -lm -o $@ $^
